extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var Bloco = load("res://Block.tscn")
onready var Player = load("res://Player.tscn")
var matriz = []
enum BLOCK_TYPES {EMPTY,RED,BLUE,YELLOW,GREEN,PURPLE,TEAL,GRAY}
var screen_offset = Vector2(9,182)

func _init():
	randomize()
	
			
# Called when the node enters the scene tree for the first time.
func _ready():
	var p = Player.instance()
	p.init(2,7)
	add_child(p)
	
	instantiate_blocks()
	drop_blocks()
	update_blocks()
	pass # Replace with function body.
	
func instantiate_next_row():
	var row = []
	for i in range(6):
		var tipo = randi()%6+1
		var bl = Bloco.instance()
		bl.init(tipo)
		row.append(bl)
	return row
	
func instantiate_blocks():
	for i in range(6):
		matriz.append([])
		for j in range(12):
			var bl = null
			if j < 6:
				var tipo = randi()%7
				if tipo != BLOCK_TYPES.EMPTY:
					bl = Bloco.instance()
					bl.init(tipo)
					update_block_pos(bl,i,j)
					add_child(bl)
			matriz[i].append(bl)
				
func update_block_pos(bloco, i, j):
	bloco.position.x = screen_offset.x + (i*16)
	bloco.position.y = screen_offset.y - (j*16)
	
func update_blocks():
	for i in range(6):
		for j in range(12):
			var bl = matriz[i][j]
			if bl != null:
				update_block_pos(bl,i,j)
				
func drop_blocks():
	for i in range(6):
		var col = []
		for j in range(12):
			var val = matriz[i][j]
			if val != null:
				col.append(val)
		var emptySpaces = 12 - len(col)
		for h in emptySpaces:
			col.append(null)
		matriz[i] = col
		
func check_blocks():
	
	for j in range(12):
		for i in range(6):
			
	for i in range(6):
		for j in range(12):
			

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
