extends Node

enum BLOCK_TYPES {EMPTY,RED,BLUE,YELLOW,GREEN,PURPLE,TEAL,GRAY}
var tipo = BLOCK_TYPES.RED
var nome = BLOCK_TYPES.keys()[BLOCK_TYPES.RED] 

func _init():
	pass
	
func init(t):
	tipo = t
	nome = BLOCK_TYPES.keys()[t] 
# Called when the node enters the scene tree for the first time.
func _ready():
	idle()
	pass # Replace with function body.

func idle():
	$AnimatedSprite.play(nome+"idle");
	
func drop():
	$AnimatedSprite.play(nome+"drop");
	
func kill():
	$AnimatedSprite.play(nome+"kill");
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
