extends Node2D
var delay_max = 0.1
var delay = delay_max
var matrix_pos = Vector2(0,0)
var cursor_offset = Vector2(16,16)
var screen_offset = Vector2(9,182)
var matrix_limits = Vector2(4,12)

func _init():
	pass
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	
func init(x,y):
	position.x = screen_offset.x + (x * 16) - cursor_offset.x
	position.y = screen_offset.y - (y * 16) + cursor_offset.y
	matrix_pos = Vector2(x,y)

func _process(delta):
	var direction = Vector2(0,0)  # The player's movement vector.
	
	if delay <= 0:
		if Input.is_action_pressed("ui_right"):
			delay = delay_max
			direction.x += 1
		if Input.is_action_pressed("ui_left"):
			delay = delay_max
			direction.x -= 1
		if Input.is_action_pressed("ui_down"):
			delay = delay_max
			direction.y += 1
		if Input.is_action_pressed("ui_up"):
			delay = delay_max
			direction.y -= 1
	else:
		delay -= delta
		
	if matrix_pos.x + direction.x <= matrix_limits.x && matrix_pos.x + direction.x >= 0:
		position.x = position.x + ( direction.x * 16)
		matrix_pos.x += + direction.x
		
	if matrix_pos.y + direction.y <= matrix_limits.y && matrix_pos.y + direction.y > 0:
		position.y = position.y + ( direction.y * 16)
		matrix_pos.y += + direction.y
